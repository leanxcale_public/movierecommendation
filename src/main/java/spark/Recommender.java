package spark;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.explode;

import com.leanxcale.spark.LeanxcaleDataSource;
import java.util.Collections;
import loader.DataLoader;
import org.apache.spark.ml.evaluation.RegressionEvaluator;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.recommendation.ALS;
import org.apache.spark.ml.recommendation.ALSModel;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.ml.tuning.TrainValidationSplit;
import org.apache.spark.ml.tuning.TrainValidationSplitModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import userUI.RatingsRetriever;

public class Recommender {

  public static void main(String[] args){

    if (args.length != 1) {
	System.out.println("USAGE: movirecommendation.jar {LX URL}");
    }
    String LX_URL = args[0];

    SparkSession spark = SparkSession.builder().appName("Movie ALS recommender").getOrCreate();

    Dataset<Row> ratings = spark.read()
        .format(LeanxcaleDataSource.SHORT_NAME)
        //.option(LeanxcaleDataSource.CONNECTION_PROPERTIES, DataLoader.LX_URL)
        .option(LeanxcaleDataSource.CONNECTION_PROPERTIES, LX_URL)
        .option(LeanxcaleDataSource.TABLE_PARAM,DataLoader.RATINGS_TABLE)
        .load()
        .cache();

    spark.sparkContext().setCheckpointDir("/tmp/checkpoint");

    ALS als = new ALS()
        .setCheckpointInterval(2)
        .setUserCol(DataLoader.USER_ID)
        .setItemCol(DataLoader.MOVIE_ID)
        .setRatingCol(DataLoader.RATING);

    ParamMap[] paramMap = new ParamGridBuilder()
        .addGrid(als.regParam(), new double[]{0.1, 10.0})
        .addGrid(als.rank(), new int[]{8, 10})
        .addGrid(als.maxIter(), new int[]{10, 20})
        .build();

    RegressionEvaluator evaluator = new RegressionEvaluator()
        .setMetricName("rmse")
        .setLabelCol(DataLoader.RATING)
        .setPredictionCol("prediction");

    TrainValidationSplit trainValidationSplit = new TrainValidationSplit()
        .setEstimator(als)
        .setTrainRatio(0.8)
        .setEvaluator(evaluator)
        .setEstimatorParamMaps(paramMap);

    TrainValidationSplitModel bestModel = trainValidationSplit.fit(ratings);

    int numUserRatings = 20;
    Dataset<Row> personalRatings  = RatingsRetriever.retrieveRatings(numUserRatings, spark, LX_URL);

    Dataset<Row> totalRatings = ratings.union(personalRatings);

    ALSModel model = als.fit(totalRatings,bestModel.extractParamMap());
    model.setColdStartStrategy("drop");

    // Generate top 10 movie recommendations for a specified set of users
    Dataset<Row> users = spark.createDataFrame(Collections
        .singletonList(RowFactory.create(0L)),
        new StructType(new StructField[]{new StructField(als.getUserCol(),DataTypes.LongType,false,Metadata.empty())}));

    Dataset<Row> userSubsetRecs = model.recommendForUserSubset(users, 10).cache();

    Dataset<Row> result = userSubsetRecs.select(col(als.getUserCol()),explode(col("recommendations")).as("rec")).select(col(als.getUserCol()), col("rec." + als.getItemCol()),col("rec.rating").cast(
        DataTypes.IntegerType));

    result.write()
        .format(LeanxcaleDataSource.SHORT_NAME).mode(SaveMode.Overwrite)
        //.option(LeanxcaleDataSource.CONNECTION_PROPERTIES, DataLoader.LX_URL)
        .option(LeanxcaleDataSource.CONNECTION_PROPERTIES, LX_URL)
        .saveAsTable("result_recommendations");

    System.out.println(String.format("Saved %d results", result.count()));
  }
}
