package userUI;

import com.leanxcale.spark.LeanxcaleDataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import loader.DataLoader;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class RatingsRetriever {

  public static Dataset<Row> retrieveRatings(int numMovies, SparkSession spark, String LX_URL){

      StructType structType = new StructType(new StructField[]{
          new StructField(DataLoader.USER_ID,DataTypes.LongType, false, Metadata.empty()),
          new StructField(DataLoader.MOVIE_ID,DataTypes.LongType, false, Metadata.empty()),
          new StructField(DataLoader.RATING,DataTypes.IntegerType, false, Metadata.empty()),
          new StructField(DataLoader.TIMESTAMP,DataTypes.LongType, false, Metadata.empty())
          //new StructField(DataLoader.TIMESTAMP,DataTypes.TimestampType, false, Metadata.empty())
      });

    return spark.read()
        .format(LeanxcaleDataSource.SHORT_NAME)
        //.option(LeanxcaleDataSource.CONNECTION_PROPERTIES, DataLoader.LX_URL)
        .option(LeanxcaleDataSource.CONNECTION_PROPERTIES, LX_URL)
        .option(LeanxcaleDataSource.TABLE_PARAM,DataLoader.MOVIES_TABLE)
        .load()
        .sample(0.01)
        .limit(numMovies)
        .cache()
        .map((MapFunction<Row, Row>) RatingsRetriever::askUser, RowEncoder.apply(structType));
  }

  private static Row askUser(Row row){

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    System.out.print(String.format("Rate this movie: %s from 0 (not seen) to 5 (Fabulous): ", row.getString(row.fieldIndex(DataLoader.TITLE))));
    boolean responseOK = false;

    int rate = -1;
    while (!responseOK) {

      try {
        rate = Integer.valueOf(reader.readLine());
        System.out.println();

        responseOK = (rate >= 0 && rate <= 5);
      } catch (NumberFormatException | IOException e) {

        System.out.println("You must write a number between 0 and 5");
      }
    }

    return RowFactory.create(0L, row.getLong(row.fieldIndex(DataLoader.MOVIE_ID)), rate, Timestamp.valueOf(LocalDateTime.now()));
  }
}
