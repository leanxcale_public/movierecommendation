package loader;

import com.leanxcale.exception.LeanxcaleException;
import com.leanxcale.kivi.database.Field;
import com.leanxcale.kivi.database.Table;
import com.leanxcale.kivi.database.Type;
import com.leanxcale.kivi.session.Session;
import com.leanxcale.kivi.session.SessionFactory;
import com.leanxcale.kivi.session.Settings;
import com.leanxcale.kivi.tuple.Tuple;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class DataLoader {

  private static final int COMMIT_THRESHOLD = 10000;

  public static final String LX_URL = "lx://localhost:9876/movie@APP";

  public static final String USER_ID = "USER_ID";
  public static final String MOVIE_ID = "MOVIE_ID";
  public static final String RATING = "RATING";
  public static final String TIMESTAMP = "TIMESTAMP";
  public static final String RATINGS_TABLE = "RATINGS";
  public static final String MOVIES_TABLE = "MOVIES";
  public static final String TITLE = "TITLE";

  public static final String RATINGS_FILE =
      "ratings.dat";
  public static final String MOVIES_FILE =
      "movies.dat";

  public static void main(String[] args) throws IOException, LeanxcaleException {

    if (args.length != 1) {
        System.out.println("USAGE: movirecommendation.jar {LX URL}");
    }
    String LX_URL = args[0];

    Settings settings = Settings.parse(LX_URL);

    try(Session session=SessionFactory.newSession(settings);
        BufferedReader ratingsReader= new BufferedReader(new InputStreamReader(DataLoader.class.getResourceAsStream(RATINGS_FILE)));
        BufferedReader moviesReader= new BufferedReader(new InputStreamReader(DataLoader.class.getResourceAsStream(MOVIES_FILE)))) {

      loadTable(session,ratingsReader,DataLoader::getRatingsTable, DataLoader::readRatingsTuple);
      loadTable(session,moviesReader,DataLoader::getMoviesTable, DataLoader::readMoviesTuple);
    }
  }

  private static void loadTable(Session session, BufferedReader reader, Function<Session, Table> getTableFunction, BiConsumer<Tuple, String> fillTupleFunction) throws IOException {

    int totalCount = 0;

    Table table = getTableFunction.apply(session);
    Tuple tuple = table.createTuple();

    String line;

    int commitCount = 0;

    while((line = reader.readLine())!=null){

      fillTupleFunction.accept(tuple,line);
      table.insert(tuple);

      totalCount++;
      commitCount++;

      if(commitCount>=COMMIT_THRESHOLD){

        session.commit();

        commitCount = 0;

        System.out.println("Commit done at: " + totalCount);
      }
    }

    if(commitCount>0){

      session.commit();
      System.out.println("Commit done at: " + totalCount);
    }

    System.out.println("Total rows inserted: " + totalCount);
  }

  private static Table getRatingsTable(Session session){

    System.out.println("Creating ratings table");

    if(session.database().tableExists(RATINGS_TABLE)){

      session.database().dropTable(RATINGS_TABLE);
    }

    Table table = session.database().createTable(RATINGS_TABLE,
        Arrays.asList(new Field(USER_ID, Type.LONG), new Field(MOVIE_ID, Type.LONG)),
        Arrays.asList(new Field(RATING, Type.INT),new Field(TIMESTAMP, Type.LONG))
        //Arrays.asList(new Field(RATING, Type.INT),new Field(TIMESTAMP, Type.TIMESTAMP))
        );

    return table;
  }

  private static void readRatingsTuple(Tuple tuple, String line){

    String[] fields = line.split("::");

    tuple.put(USER_ID, Long.valueOf(fields[0]));
    tuple.put(MOVIE_ID, Long.valueOf(fields[1]));
    tuple.put(RATING, Integer.valueOf(fields[2]));
    tuple.put(TIMESTAMP, (Long.valueOf(fields[0])*1000));
    //tuple.put(TIMESTAMP, new Timestamp(Long.valueOf(fields[0])*1000));

  }

  private static Table getMoviesTable(Session session){

    System.out.println("Creating movies table");

    if(session.database().tableExists(MOVIES_TABLE)){

      session.database().dropTable(MOVIES_TABLE);
    }

    Table table = session.database().createTable(MOVIES_TABLE,
        Collections.singletonList(new Field(MOVIE_ID, Type.LONG)),
        Collections.singletonList(new Field(TITLE, Type.STRING))
    );

    return table;
  }

  private static void readMoviesTuple(Tuple tuple, String line){

    String[] fields = line.split("::");

    tuple.put(MOVIE_ID, Long.valueOf(fields[0]));
    tuple.put(TITLE, fields[1]);
  }
}
